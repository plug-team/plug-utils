package plug.utils.tests;

import org.junit.Test;
import plug.utils.CartesianProduct;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

/**
 * Created by Ciprian TEODOROV on 09/03/17.
 */
public class CartesianProductTest {
    @Test
    public void testCubePrint() {
        int cubeSize = 3;
        int[] array = new int[cubeSize];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }

        new CartesianProduct().foreach(array, cubeSize, (Function<int[], Integer>) CartesianProduct::printAllTuplesCallback);
    }

    @Test
    public void testPrintAll() {

        int[] array = new int[7];
        for (int i = 0; i < array.length; i++) {
            array[i] = i;
        }

        new CartesianProduct().foreach(array, 3, (Function<int[], Integer>) CartesianProduct::printAllTuplesCallback);
    }

    @Test
    public void testCartesianProduct() {
        Integer[] a = new Integer[2];
        Integer[] b = new Integer[3];
        Integer[] c = new Integer[4];

        for (int i = 0; i < a.length; i++) {
            a[i] = i;
        }
        for (int i = 0; i < b.length; i++) {
            b[i] = i;
        }
        for (int i = 0; i < c.length; i++) {
            c[i] = i;
        }

        List<List<Integer>> listOfList = new ArrayList<>();
        listOfList.add(Arrays.asList(b));
        listOfList.add(Arrays.asList(c));
        listOfList.add(Arrays.asList(a));

        CartesianProduct<Integer> cp = new CartesianProduct<>();
        cp.cartesianProduct(listOfList, this::printIntegerTuple, Integer[]::new);
    }

    void printIntegerTuple(Integer[] tuple) {
        System.out.println(Arrays.toString(tuple));
    }

    @Test
    public void testCartesianProduct1() {
        Integer[] a = new Integer[2];
        Integer[] b = new Integer[3];
        Integer[] c = new Integer[4];

        for (int i = 0; i < a.length; i++) {
            a[i] = i;
        }
        for (int i = 0; i < b.length; i++) {
            b[i] = i;
        }
        for (int i = 0; i < c.length; i++) {
            c[i] = i;
        }

        Integer[][] arr = new Integer[3][];
        arr[0] = b;
        arr[1] = c;
        arr[2] = a;
        CartesianProduct<Integer> cp = new CartesianProduct<>();
        cp.cartesianProduct(arr, this::printIntegerTuple, new Integer[3]);
    }

    void printIntegerTuple(Integer[][] tuple) {
        System.out.println(Arrays.toString(tuple[0]) + " --- " + Arrays.toString(tuple[1]));
    }

    @Test
    public void testCartesianProduct2set() {
        Integer[] a = new Integer[2];
        Integer[] b = new Integer[3];
        Integer[] c = new Integer[4];

        for (int i = 0; i < a.length; i++) {
            a[i] = i;
        }
        for (int i = 0; i < b.length; i++) {
            b[i] = i;
        }
        for (int i = 0; i < c.length; i++) {
            c[i] = i;
        }

        Integer[][] arr = new Integer[3][];
        arr[0] = b;
        arr[1] = c;
        arr[2] = a;
        CartesianProduct<Integer> cp = new CartesianProduct<>();
        cp.cartesianProduct(arr, arr, this::printIntegerTuple, new Integer[2][3]);
    }

    @Test
    public void testCartesianProduct2set1() {
        Integer[] a = new Integer[2];
        Integer[] b = new Integer[3];
        Integer[] c = new Integer[4];

        for (int i = 0; i < a.length; i++) {
            a[i] = i;
        }
        for (int i = 0; i < b.length; i++) {
            b[i] = i;
        }
        for (int i = 0; i < c.length; i++) {
            c[i] = i;
        }

        Integer[][] arr = new Integer[3][];
        arr[0] = b;
        arr[1] = c;
        arr[2] = a;
        CartesianProduct<Integer> cp = new CartesianProduct<>();

        Integer[][] resultHolder0 = new Integer[2][];
        resultHolder0[0] = new Integer[3];

        cp.cartesianProduct(arr, new Integer[0][0], this::printIntegerTuple, resultHolder0);

        Integer[][] resultHolder1 = new Integer[2][];
        resultHolder1[1] = new Integer[3];
        cp.cartesianProduct(new Integer[0][0], arr, this::printIntegerTuple, resultHolder1);
    }
}
