package plug.utils;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

public class ObjectGraphExploration {
    Map<Object, Class<?>> closed = new IdentityHashMap<>();
    ArrayDeque<StackEntry> open = new ArrayDeque<>();

    IVisitor visitor;

    boolean excludeTransient = true;
    boolean excludeStatic = true;

    private final Set<Class<?>> excludedClasses = Collections.newSetFromMap(new IdentityHashMap<>());

    public ObjectGraphExploration(IVisitor visitor) {
        this.visitor = visitor;
    }

    public ObjectGraphExploration visitor(IVisitor visitor) {
        this.visitor = visitor;
        return this;
    }

    public interface IVisitor {
        /**
         * Called for each Object visited.
         *
         * @param object The object being visited
         * @return return true if you wish the graph transversal to stop, otherwise it will continue.
         */
        boolean startObject(Object object, Class<?> clazz);
        boolean endObject(Object object, Class<?> clazz);

        boolean startPrimitive(Object object, Class<?> clazz);
        boolean endPrimitive(Object object, Class<?> clazz);

        boolean startArray(Object object, Class<?> clazz);
        boolean endArray(Object object, Class<?> clazz);

        boolean startField(Field field, Object object, Class<?> clazz);
        boolean endField(Field field, Object object, Class<?> clazz);
    }

    /**
     * Include transient fields. By default they are excluded.
     *
     * @return this
     */
    public ObjectGraphExploration includeTransient() {
        excludeTransient = false;
        return this;
    }

    /**
     * Exclude transient fields. By default they are excluded.
     *
     * @return this
     */
    public ObjectGraphExploration excludeTransient() {
        excludeTransient = true;
        return this;
    }

    /**
     * Include static fields. By default they are excluded.
     *
     * @return this
     */
    public ObjectGraphExploration includeStatic() {
        excludeStatic = false;
        return this;
    }

    /**
     * Exclude static fields. By default they are excluded.
     *
     * @return this
     */
    public ObjectGraphExploration excludeStatic() {
        excludeStatic = true;
        return this;
    }

    /**
     * Exclude any object that extends from these classes.
     *
     * @param classes to exclude.
     * @return this
     */
    public ObjectGraphExploration excludeClasses(Class<?>... classes) {
        for (Class<?> c : classes) {
            if (c == null) {
                throw new NullPointerException("Null class not allowed");
            }
            excludedClasses.add(c);
        }
        return this;
    }

    /**
     * Is the class on the excluded list.
     *
     * @param clazz
     * @return
     */
    private boolean isExcludedClass(Class<?> clazz) {
        return excludedClasses.stream().anyMatch(c -> c.isAssignableFrom(clazz));
    }

    public void explore(Object root, Class<?> clazz) {
        //reset the state
        closed.clear();
        open.clear();

        //initialize open
        closed.put(root, clazz);
        open.push(new StackEntry(root));

        run();
    }

    /**
     * Add this object to be visited if it has not already been closed.
     *
     * @param o The object
     */
    boolean addIfNotClosed(Object o, Class<?> clazz) {
        if (o instanceof StackEntry) {
            StackEntry se = (StackEntry) o;
            Class<?> notInYet = closed.put(se.field, clazz);
            if (notInYet == null) {
                open.push(se);
            }
            return notInYet == null;
        }
        Class<?> notInYet = closed.put(o, clazz);
        if (notInYet == null) {
            open.push(new StackEntry(o));
        }
        return notInYet == null;
    }

    void run() {
        boolean isDone = false;
        boolean stepIn = true;
        while (!isDone && !open.isEmpty()) {
            StackEntry entry = open.peek();
            Object current = entry.object;
            Class<?> clazz = closed.get(current);

            Field field = entry.field;

            if (field != null) {
                if (entry.fieldDone) {
                    open.pop();
                    isDone = visitor.endField(field, current, clazz);
                    continue;
                }
                //handle field
                entry.fieldDone = true;
                Class<?> fieldType = field.getType();

                try {
                    field.setAccessible(true);

                    stepIn = visitor.startField(field, current, fieldType);
                    if (!stepIn) continue;
                    Object value = field.get(current);

                    // If the object's type, or parent of the object's type is on the exclude list, then
                    // skip.
                    if (value != null && isExcludedClass(value.getClass())) continue;

                    addIfNotClosed(value, fieldType);

                } catch (IllegalAccessException e) {
                    // Ignore the exception
                }
                continue;
            }

            Iterator postIterator = entry.post;
            if (postIterator == null) {
                //no post iterator, means the state is open
                if (clazz.isPrimitive()) {
                    stepIn = visitor.startPrimitive(current, clazz);
                    entry.setPost(postIterator = Collections.emptyIterator());
                } else if (clazz.isArray()) {
                    stepIn = visitor.startArray(current, clazz);
                    if (!stepIn) {
                        entry.setPost(postIterator = Collections.emptyIterator());
                    }
                    else {
                        Iterator iterator = new Iterator() {
                            final int size = Array.getLength(current);
                            int i = 0;

                            @Override
                            public boolean hasNext() {
                                return i < size;
                            }

                            @Override
                            public Object next() {
                                return Array.get(current, i++);
                            }
                        };
                        entry.setPost(postIterator = iterator);
                    }
                } else {
                    stepIn = visitor.startObject(current, clazz);
                    if (!stepIn) {
                        entry.setPost(postIterator = Collections.emptyIterator());
                    }
                    else {
                        List<Field> fields = getAllFields(current.getClass());
                        entry.setPost(postIterator = fields.iterator());
                    }
                }
            }

            if (postIterator.hasNext()) {
                Object target = postIterator.next();

                if (target instanceof Field) {
                    //get the field
                    field = (Field)target;
                    int modifiers = field.getModifiers();

                    if (excludeStatic && (modifiers & Modifier.STATIC) == Modifier.STATIC) continue;

                    if (excludeTransient && (modifiers & Modifier.TRANSIENT) == Modifier.TRANSIENT) continue;

                    Class<?> fieldType = field.getType();

                    addIfNotClosed(new StackEntry(field, current), fieldType);
                } else {
                    //can be only array
                    Class<?> arrayType = clazz.getComponentType();
                    addIfNotClosed(target, arrayType);
                }
            } else {
                open.pop();
                if (clazz.isPrimitive()) {
                    isDone = visitor.endPrimitive(current, clazz);
                } else if (clazz.isArray()) {
                    isDone = visitor.endArray(current, clazz);
                } else {
                    isDone = visitor.endObject(current, clazz);
                }
            }
        }
    }

    Map<Class<?>, List<Field>> fieldCache = new IdentityHashMap<>();

    /**
     * @param clazz the class
     * @return all declared and inherited fields for this class.
     */
    private List<Field> getAllFields(Class<?> clazz) {
        List<Field> fields = fieldCache.get(clazz);
        if (fields != null) return fields;

        fields = new ArrayList<>(Arrays.asList(clazz.getDeclaredFields()));

        if (clazz.getSuperclass() != null) {
            fields.addAll(getAllFields(clazz.getSuperclass()));
        }

        return fields;
    }

    class StackEntry {
        Object object;
        Field field;
        boolean fieldDone = false;
        Iterator<Object> post;

        StackEntry(Object object) {
            this(null, object);
        }

        StackEntry(Field field, Object object) {
            this.field = field;
            this.object = object;
        }

        public void setPost(Iterator<Object> post) {
            this.post = post;
        }
    }
}