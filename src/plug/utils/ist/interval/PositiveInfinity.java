package plug.utils.ist.interval;

public class PositiveInfinity extends UnboundedConstraint {
    public static PositiveInfinity PINF = new PositiveInfinity();

    @Override
    public boolean lt(Constraint other) {
        return false;
    }

    @Override
    public boolean le(Constraint other) {
        return false;
    }

    @Override
    public Constraint max(Constraint other) {
        return this;
    }

    @Override
    public Constraint min(Constraint other) {
        return other;
    }
}
