package plug.utils.ist.interval;

public class InclusiveConstraint extends BoundedConstraint {
    public InclusiveConstraint(int bound) {
        super(bound);
    }

    @Override
    public boolean isInclusive() {
        return true;
    }
}
