package plug.utils.ist.interval;

public abstract class UnboundedConstraint extends Constraint {
    @Override
    boolean isBounded() {
        return false;
    }

    @Override
    boolean isInclusive() {
        return false;
    }

    @Override
    public Constraint add(Constraint other) {
        return this;
    }

    @Override
    public Constraint add(int value) {
        return this;
    }

    @Override
    public Constraint subtract(Constraint other) {
        return this;
    }

    @Override
    public Constraint subtract(int value) {
        return this;
    }
}
