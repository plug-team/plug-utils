package plug.utils.ist.interval;

public  class BoundedConstraint extends Constraint {
    int bound;
    public BoundedConstraint(int bound) {
        this.bound = bound;
    }

    @Override
    public boolean lt(Constraint other) {
        if (other.isUnbounded()) {
            return other.lt(this);
        }
        BoundedConstraint bother = (BoundedConstraint)other;
        if (this.bound == bother.bound) return this.isInclusive() && bother.isExclusive();
        return this.bound < ((BoundedConstraint)other).bound;
    }

    @Override
    public boolean le(Constraint other) {
        return false;
    }

    @Override
    public Constraint add(Constraint other) {
        return null;
    }

    @Override
    public Constraint add(int value) {
        return null;
    }

    @Override
    public Constraint subtract(Constraint other) {
        return null;
    }

    @Override
    public Constraint subtract(int value) {
        return null;
    }

    @Override
    public Constraint max(Constraint other) {
        return null;
    }

    @Override
    public Constraint min(Constraint other) {
        return null;
    }

    @Override
    boolean isBounded() {
        return true;
    }

    @Override
    boolean isInclusive() {
        return false;
    }
}
