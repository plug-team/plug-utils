package plug.utils.ist.interval;

public class NegativeInfinity extends UnboundedConstraint {
    public static NegativeInfinity NINF = new NegativeInfinity();

    @Override
    public boolean lt(Constraint other) {
        return true;
    }

    @Override
    public boolean le(Constraint other) {
        return true;
    }

    @Override
    public Constraint max(Constraint other) {
        return other;
    }

    @Override
    public Constraint min(Constraint other) {
        return this;
    }
}
