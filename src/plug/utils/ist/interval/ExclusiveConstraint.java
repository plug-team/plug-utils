package plug.utils.ist.interval;

public class ExclusiveConstraint extends BoundedConstraint {
    public ExclusiveConstraint(int bound) {
        super(bound);
    }

    @Override
    public boolean isInclusive() {
        return false;
    }
}
