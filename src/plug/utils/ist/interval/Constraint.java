package plug.utils.ist.interval;

public abstract class Constraint {
    public static Constraint PINF = PositiveInfinity.PINF;
    public static Constraint NINF = NegativeInfinity.NINF;

    //Returns true if (this > other)
    public boolean gt(Constraint other) {
        return other.lt(this);
    }
    //Returns true if (this >= other)
    public boolean ge(Constraint other) {
        return other.le(this);
    }
    //Returns true if (this < other)
    public abstract boolean lt(Constraint other);
    //Returns true if (this <= other)
    public abstract boolean le(Constraint other);
    //Returns a Constraint whose value is (this + other).
    public abstract Constraint add(Constraint other);
    //Returns a Constraint whose value is (this + value).
    public abstract Constraint add(int value);
    //Returns a Constraint whose value is (this - other).
    public abstract Constraint subtract(Constraint other);
    //Returns a Constraint whose value is (this - value).
    public abstract Constraint subtract(int value);
    //Returns the maximum of this and other.
    public abstract Constraint max(Constraint other);
    //Returns the minimum of this and other.
    public abstract Constraint min(Constraint other);

    abstract boolean isBounded();

    public boolean isUnbounded() {
        return !isBounded();
    }

    abstract boolean isInclusive();

    public boolean isExclusive() {
        return !isInclusive();
    }
}
