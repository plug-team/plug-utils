package plug.utils.ist.interval;

/*
* Ganty, Pierre, Cédric Meuter, Laurent Van Begin, Gabriel Kalyon, Jean-François Raskin, and Giorgio Delzanno.
* Symbolic data structure for sets of k-uples of integers.
* Technical Report 570, Département d’Informatique-Université Libre de Bruxelles, 2006.
* */

public class Interval {
    public static Interval EMPTY = new Interval(new ExclusiveConstraint(0), new ExclusiveConstraint(0));

    Constraint leftBound;
    Constraint rightBound;

    public Interval(Constraint leftBound, Constraint rightBound) {
        if (leftBound.gt(rightBound)) {
            leftBound = new ExclusiveConstraint(0);
            rightBound = new ExclusiveConstraint(0);
        }
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }

    public Interval singleton(int bound) {
        return new Interval(new InclusiveConstraint(bound), new InclusiveConstraint(bound));
    }

    public boolean isEmpty() {
        if (isUnbounded()) return false;

        if (leftBound.lt(rightBound)) return false;

        if (leftBound.le(rightBound)
                && !(leftBound.isInclusive() || rightBound.isInclusive()))
            return false;

        return true;
    }

    public boolean gt( Interval other) {
        if (this.leftBound.gt(other.leftBound)) {
            return true;
        }
        if (this.leftBound.equals(other.leftBound) && this.rightBound.gt(other.rightBound)) {
            return true;
        }
        return false;
    }

    public boolean ge( Interval other) {
        if (this.leftBound.ge(other.leftBound)) {
            return true;
        }
        if (this.leftBound.equals(other.leftBound) && this.rightBound.ge(other.rightBound)) {
            return true;
        }
        return false;
    }

    public boolean lt( Interval other) {
        if (this.leftBound.lt(other.leftBound)) {
            return true;
        }
        if (this.leftBound.equals(other.leftBound) && this.rightBound.lt(this.rightBound)) {
            return true;
        }
        return false;
    }

    public boolean le( Interval other) {
        if (this.leftBound.le(other.leftBound)) {
            return true;
        }
        if (this.leftBound.equals(other.leftBound) && this.rightBound.le(this.rightBound)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Interval)) {
            return false;
        }
        Interval other = (Interval) obj;
        return this == obj || (this.leftBound.equals(other.leftBound) && this.rightBound.equals(other.rightBound));
    }

    public boolean isUnbounded() {
        return isLeftUnbounded() || isRightUnbounded();
    }

    public boolean isRightUnbounded() {
        return !this.rightBound.isBounded();
    }

    public boolean isLeftUnbounded() {
        return !this.leftBound.isBounded();
    }

    public Interval assign(Interval value) {
        this.leftBound = value.leftBound;
        this.rightBound = value.rightBound;
        return this;
    }

    public Interval assign(Constraint leftBound, Constraint rightBound) {
        this.leftBound = leftBound;
        this.rightBound = rightBound;
        return this;
    }

    public boolean includes(Interval other) {
        return this.leftBound.le(other.leftBound) && this.rightBound.ge(other.rightBound);
    }

    public boolean intersects(Interval other) {
        if (this.isEmpty() || other.isEmpty()) return false;

        if (rightBound.lt(other.leftBound) || other.rightBound.lt(leftBound)) return false;

        return true;
    }

    public Interval intersection(Interval other) {
        if (this.isEmpty() || other.isEmpty()) return Interval.EMPTY;

        if (rightBound.lt(other.leftBound) || other.rightBound.lt(leftBound)) return Interval.EMPTY;

        return new Interval(leftBound.max(other.leftBound), rightBound.min(other.rightBound));
    }

    public Interval union (Interval interval) {
        if (this.includes(interval)) return this;
        if (interval.includes(this)) return interval;

        return new Interval(leftBound.min(interval.leftBound), rightBound.max(interval.rightBound));

    }

    public Interval add(int value) {
        leftBound.add(value);
        rightBound.add(value);
        return this;
    }

    public Interval add(Interval interval) {
        leftBound.add(interval.leftBound);
        rightBound.add(interval.rightBound);
        return this;
    }

    public Interval subtract(int value) {
        leftBound.subtract(value);
        rightBound.subtract(value);
        return this;
    }

    public Interval subtract(Interval interval) {
        leftBound.subtract(interval.leftBound);
        rightBound.subtract(interval.rightBound);
        return this;
    }



    public Interval copy() {
        return new Interval(leftBound, rightBound);
    }
}
