package plug.utils.hash;

public class UniversalHashedInt {
	int value;
	public static int a, b;
	public static int p;
	public static int m;
	
	public UniversalHashedInt(int value) {
		this.value = value;
	}
	
	@Override
	public boolean equals(Object b) {
		if (b instanceof UniversalHashedInt)
			return this.value == ((UniversalHashedInt)b).value;
		return false;
	}
	
	@Override
	public int hashCode() {
		return (((a*value + b) % p) % m);
	}
	
	@Override
	public String toString(){
		return value + "@hash[" + hashCode() + "]";
	}
}
