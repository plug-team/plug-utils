package plug.utils;

import java.io.Serializable;

/**
 * Created by ciprian on 16/02/17.
 */
public class Pair<A, B> implements Serializable {
    public A a;
    public B b;

    public Pair(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(!(obj instanceof Pair)) return false;

        Pair other = (Pair)obj;
        return this.a.equals(other.a) && this.b.equals(other.b);
    }

    public int hashCode() {
        int result = 31 + a.hashCode();
        result = 31 * result + b.hashCode();
        return result;
    }

    public String toString() {
        return String.format("(%s, %s)", new Object[]{this.a, this.b});
    }
}
