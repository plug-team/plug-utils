package plug.utils.file;

import com.sun.nio.file.SensitivityWatchEventModifier;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public class FileWatcher {

    protected Thread watchThread;

    protected Map<Path, WatchKey> pathWatchKeys = new HashMap<>();
    protected Map<Path, Set<Consumer<Path>>> pathListener = new HashMap<>();

    protected WatchService watchService;

    public FileWatcher() throws IOException {
        watchService = FileSystems.getDefault().newWatchService();
        watchThread = new Thread(this::watcher);
        watchThread.start();
    }

    protected void watcher() {
        // infinite loop until something happen
        try {
            WatchKey key;
            while ((key = watchService.take()) != null) {
                Path parent = (Path) key.watchable();
                for (WatchEvent<?> event : key.pollEvents()) {
                    Path path = parent.resolve((Path) event.context());
                    Set<Consumer<Path>> consumers = pathListener.get(path);
                    if (consumers != null) {
                        for (Consumer<Path> consumer : consumers) {
                            consumer.accept(path);
                        }
                    }
                }
                key.reset();
            }
        } catch (Throwable e) {
            e.printStackTrace();
            // nothing to do
        }
    }

    public synchronized void addOnContentChange(Path path, Consumer<Path> onChange) {
        if (path == null) return;
        // create watch key if needed
        Path directoryPath = Files.isDirectory(path) ? path : path.getParent();
        pathWatchKeys.computeIfAbsent(directoryPath, (p) -> {
            try {
                return directoryPath.register(watchService,
                        new WatchEvent.Kind[]{StandardWatchEventKinds.ENTRY_MODIFY},
                        SensitivityWatchEventModifier.HIGH
                );
            } catch (IOException e) {
                //TODO: handleError("Cannot register file '"+ path +"' to watch", e);
                return null;
            }
        });

        Set<Consumer<Path>> consumers = pathListener.computeIfAbsent(path, (p) -> new HashSet<>());
        consumers.add(onChange);
    }

    public synchronized void removeOnContentChange(Path path, Consumer<Path> onChange) {
        Set<Consumer<Path>> consumers = pathListener.get(path);
        if (consumers != null && consumers.contains(onChange)) {
            consumers.remove(onChange);
            if (consumers.isEmpty()) {
                pathListener.remove(path);
                pathWatchKeys.remove(path);
            }
        }
    }

    public synchronized void removePathListener(Path path) {
        pathListener.remove(path);
        WatchKey key = pathWatchKeys.get(path);
        if (key != null) key.cancel();
        pathWatchKeys.remove(path);
    }


}
