package plug.utils.exec;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A Process Runner starts an external process with a given command.
 * It redirects the streams into given writers and accept env variables
 * and input stream for contents.
 *
 * @author Jean-Charles Roger (jean-charles.roger@ensieta.fr).
 */
public class ProcessRunner {

	private final PrintWriter out;
	private final PrintWriter err;
	
	private Path workingPath;

	/** If true, print the command output to out */
	private boolean verbose = true;
	
	/** Env to pass to external process. */
	private String[] env = null;

	private long processStartDate;

	private Thread pumperOut;
	private Thread pumperErr;
	private Thread pumperIn;

	private Process currentProcess;

	public ProcessRunner(PrintWriter out, PrintWriter err) {
		this.out = out;
		this.err = err;
		workingPath = Paths.get(System.getProperty("user.dir"));
	}

	public boolean isVerbose() {
		return verbose;
	}
	
	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
	
	public Path getWorkingPath() {
		return workingPath;
	}
	
	public void setWorkingPath(Path workingPath) {
		this.workingPath = workingPath;
	}
	
	public String[] getEnv() {
		return env;
	}
	
	public void setEnv(String[] env) {
		this.env = env;
	}

	public void println(String message) {
		out.println(message);
	}

	public void startProcess(String[] command, InputStream inContentsStream) {
		
		// print log
		StringBuilder commandToPrint = new StringBuilder();
		commandToPrint.append("[");
		for ( String arg : command ) {
			if ( commandToPrint.length() > 1) commandToPrint.append(" ");
			commandToPrint.append(arg);
		}
		commandToPrint.append("]");
		out.println("Exec: " + commandToPrint);
		
		currentProcess = null;
		final StringBuilder outBuffer = new StringBuilder();
		try {
			processStartDate = System.currentTimeMillis();
			currentProcess = Runtime.getRuntime().exec(command, env, getWorkingPath().toFile());
			
			final BufferedReader outReader = new BufferedReader(new InputStreamReader(currentProcess.getInputStream()));
			final BufferedReader errReader = new BufferedReader(new InputStreamReader(currentProcess.getErrorStream()));
			
			// uses a Thread to read the output buffer.
			// Windows problem:
			// The output buffer has a limited size, it must be read or the process will be blocked.
			// See http://support.microsoft.com/kb/326709/fr
			// saves output in a buffer
			// stop the thread
			pumperOut = new Thread(() -> {
				try {
					String line = outReader.readLine();
					while (line != null ) {
						// saves output in a buffer
						outBuffer.append(line);
						outBuffer.append("\n");

						if (verbose) {
							out.println(line);
						}
						line = outReader.readLine();
					}
				} catch (Exception e) {
					// stop the thread
					e.printStackTrace();
				}
				out.flush();
			}, "StdOut Pumper");
			pumperOut.start();

			// stop the thread
			pumperErr = new Thread(() -> {
				try {
					String line = errReader.readLine();
					while (line != null ) {
						err.println(line);
						line = errReader.readLine();
					}
				} catch (Exception e) {
					// stop the thread
					e.printStackTrace();
				}
				err.flush();
			}, "StdErr Pumper");
			pumperErr.start();

			pumperIn = null;
			if ( inContentsStream != null ) {
				final BufferedInputStream bufferedInContentsStream = new BufferedInputStream(inContentsStream);
				final BufferedOutputStream bufferedInStream = new BufferedOutputStream(currentProcess.getOutputStream());
				pumperIn = new Thread(() -> {
					try {
						byte[] buffer = new byte[1024];
						int read = 0;
						while ( read >= 0 ) {
							bufferedInStream.write(buffer, 0, read);
							read = bufferedInContentsStream.read(buffer);
						}
					} catch (Exception e) {
						// stop the thread
						e.printStackTrace();

					} finally {
						try {
							bufferedInContentsStream.close();
							bufferedInStream.close();
						} catch (Exception e) {
							// stop the thread
							e.printStackTrace();
						}
					}
				}, "StdIn Pumper");
				pumperIn.start();
			}
			

			
		} catch (Exception e) {
			out.println(outBuffer.toString());
		}
	}

	public boolean waitForProcess() {
		try {
			if (pumperIn != null) pumperIn.join();
			pumperOut.join();
			currentProcess.waitFor();
			pumperErr.join();

		} catch (InterruptedException e ) {
			kill();
			return false;
		}

		long processEndDate = System.currentTimeMillis();
		double duration = (processEndDate - processStartDate) / 1e3;

		int status = currentProcess.exitValue();

		out.print("Process last " + duration + "s and exit with " + status + ".\n");
		out.flush();
		return status != 0;
	}

	public void kill() {
		if ( currentProcess != null ) {
			currentProcess.destroy();
			currentProcess = null;
		}
	}
}
