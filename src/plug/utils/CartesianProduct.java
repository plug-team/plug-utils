package plug.utils;

/**
 * Created by Ciprian TEODOROV on 09/03/17.
 */

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;


public class CartesianProduct<T> {

    public void cartesianProduct(List[] set1, List[] set2, Consumer<T[][]> userCallback, T[][] resultHolder) {
        int maxSet1 = 0;
        for (List l : set1) {
            if (l.size() > maxSet1) maxSet1 = l.size();
        }

        int maxSet2 = 0;
        for (List l : set2) {
            if (l.size() > maxSet2) maxSet2 = l.size();
        }
        int[] internalTuple2 = new int[set2.length];

        if (set1.length == 0) {
            Consumer<T[]> callback2 = (tuple2) -> {
                userCallback.accept(resultHolder);
            };
            cartesianProduct(set2, maxSet2, internalTuple2, callback2, resultHolder[1]);
            return;
        }

        final int maxSet2final = maxSet2;
        Consumer<T[]> callback1 = (tuple1) -> {
            if (set2.length == 0) {
                userCallback.accept(resultHolder);
                return;
            }
            Consumer<T[]> callback2 = (tuple2) -> {
                userCallback.accept(resultHolder);
            };
            cartesianProduct(set2, maxSet2final, internalTuple2, callback2, resultHolder[1]);
        };

        cartesianProduct(set1, maxSet1, new int[set1.length], callback1, resultHolder[0]);
    }

    //This should be the fastest version
    @SuppressWarnings( "unchecked" )
    public void cartesianProduct(List[] arrayOfLists, int max, int[] internalTuple, Consumer<T[]> userCallback, T[] resultHolder) {
        int setCardinality = arrayOfLists.length;

        Function<int[], Integer> pruning = (tuple) -> {
            int stopAt = setCardinality;

            for (int idx = 0; idx < tuple.length; idx++) {
                List theList = arrayOfLists[idx];
                stopAt = (theList.size() - 1 < tuple[idx]) ? idx : stopAt;
                if (stopAt == setCardinality) {
                    resultHolder[idx] = (T)theList.get(tuple[idx]);
                }
            }
            if (stopAt == setCardinality) {
                userCallback.accept(resultHolder);
            }

            return stopAt;
        };

        foreach(max, setCardinality, internalTuple, pruning);
    }

    //This should be the fastest version
    @SuppressWarnings( "unchecked" )
    public void cartesianProduct(List[] arrayOfLists, Consumer<T[]> userCallback, T[] resultHolder) {
        int setCardinality = arrayOfLists.length;

        int max = 0;
        for (List l : arrayOfLists) {
            if (l.size() > max) max = l.size();
        }

        Function<int[], Integer> pruning = (tuple) -> {
            int stopAt = setCardinality;

            for (int idx = 0; idx < tuple.length; idx++) {
                List theList = arrayOfLists[idx];
                stopAt = (theList.size() - 1 < tuple[idx]) ? idx : stopAt;
                if (stopAt == setCardinality) {
                    resultHolder[idx] = (T)theList.get(tuple[idx]);
                }
            }
            if (stopAt == setCardinality) {
                userCallback.accept(resultHolder);
            }

            return stopAt;
        };

        foreach(max, setCardinality, new int[setCardinality], pruning);
    }

    public void cartesianProduct(T[][] set1, T[][] set2, Consumer<T[][]> userCallback, T[][] resultHolder) {
        if (set1.length == 0) {
            Consumer<T[]> callback2 = (tuple2) -> {
                userCallback.accept(resultHolder);
            };
            cartesianProduct(set2, callback2, resultHolder[1]);
            return;
        }

        Consumer<T[]> callback1 = (tuple1) -> {
            if (set2.length == 0) {
                userCallback.accept(resultHolder);
                return;
            }
            Consumer<T[]> callback2 = (tuple2) -> {
                userCallback.accept(resultHolder);
            };
            cartesianProduct(set2, callback2, resultHolder[1]);
        };

        cartesianProduct(set1, callback1, resultHolder[0]);
    }

    //This should be the fastest version
    public void cartesianProduct(T[][] arrayOfArrays, Consumer<T[]> userCallback, T[] resultHolder) {
        int setCardinality = arrayOfArrays.length;

        int max = 0;
        for (T[] l : arrayOfArrays) {
            if (l.length > max) max = l.length;
        }

        Function<int[], Integer> pruning = (tuple) -> {
            int stopAt = setCardinality;

            for (int idx = 0; idx < tuple.length; idx++) {
                T[] theList = arrayOfArrays[idx];
                stopAt = (theList.length - 1 < tuple[idx]) ? idx : stopAt;
                if (stopAt == setCardinality) {
                    resultHolder[idx] = theList[tuple[idx]];
                }
            }
            if (stopAt == setCardinality) {
                userCallback.accept(resultHolder);
            }

            return stopAt;
        };

        foreach(max, setCardinality, new int[setCardinality], pruning);
    }

    public void foreach(int setCardinality, int inArity, int[] tuple, Function<int[], Integer> pruningCallback) {
        long resultCardinality = (int) Math.pow(setCardinality, inArity);
        long[] precomputedScale = scalesForArity(inArity, setCardinality, resultCardinality);
        //int[] tuple = new int[inArity];

        long idx = 0;
        while (idx < resultCardinality) {
            int skipIndex;

            //generate the idx th solution
            for (int tupleIdx = 0; tupleIdx < inArity; tupleIdx++) {
                int position = (int) Math.floor(idx / (precomputedScale[tupleIdx]) % setCardinality);
                tuple[tupleIdx] = position;
            }

            skipIndex = pruningCallback.apply(tuple);
            assert (skipIndex <= inArity);
            if (skipIndex == inArity) {
                idx++;
            } else {
                idx += precomputedScale[skipIndex];
            }
        }
    }

    public static Integer printAllTuplesCallback(int[] tuple) {
        System.out.print("(");
        for (int i = 0; i < tuple.length; i++) {
            if (i == tuple.length - 1) {
                System.out.print(tuple[i]);
                continue;
            }
            System.out.print(tuple[i] + ", ");
        }
        System.out.println(")");
        return tuple.length;
    }

    public static <T> List<List<T>> recursiveCartesianProduct(List<List<T>> lists) {
        List<List<T>> resultLists = new ArrayList<List<T>>();
        if (lists.size() == 0) {
            resultLists.add(new ArrayList<T>());
            return resultLists;
        } else {
            List<T> firstList = lists.get(0);
            List<List<T>> remainingLists = recursiveCartesianProduct(lists.subList(1, lists.size()));
            for (T condition : firstList) {
                for (List<T> remainingList : remainingLists) {
                    ArrayList<T> resultList = new ArrayList<T>();
                    resultList.add(condition);
                    resultList.addAll(remainingList);
                    resultLists.add(resultList);
                }
            }
        }
        return resultLists;
    }

    private class CartesianProductPruning implements Function<int[], Integer>{

        List<List<T>> listOfLists;
        T[] result;
        Consumer<T[]> userCallback;

        public Integer apply(int[] tuple) {
            int arity = listOfLists.size();
            int stopAt = arity;

            for (int idx = 0; idx < tuple.length; idx++) {
                List<T> theList = listOfLists.get(idx);
                stopAt = (theList.size() - 1 < tuple[idx]) ? idx : stopAt;
                if (stopAt == arity) {
                    result[idx] = theList.get(tuple[idx]);
                }
            }
            if (stopAt == arity) {
                userCallback.accept(result);
            }

            return stopAt;
        }
    }

    public void cartesianProduct(List<List<T>> listOfLists, Consumer<T[]> userCallback, Function<Integer, T[]> supplier) {
        int arity = listOfLists.size();
        T[] result = supplier.apply(arity);

        int max = 0;
        for (List<T> l : listOfLists) {
            if (l.size() > max) max = l.size();
        }

        int[] theArray = new int[max];
        for (int i = 0; i < max; i++) {
            theArray[i] = i;
        }

        CartesianProductPruning privateCallback = new CartesianProductPruning();
        privateCallback.listOfLists = listOfLists;
        privateCallback.result = result;
        privateCallback.userCallback = userCallback;
        foreach(theArray, arity, privateCallback);
    }

    public short foreach(int[] inArray, int inArity, Function<int[], Integer> pruningCallback) {
        int setCardinality = inArray.length;
        long resultCardinality = (int) Math.pow(setCardinality, inArity);

        long[] precomputedScale = scalesForArity(inArity, setCardinality, resultCardinality);
        int[] tuple = new int[inArity];

        long idx = 0;
        while (idx < resultCardinality) {
            int skipIndex;

            //generate the idx th solution
            for (int tupleIdx = 0; tupleIdx < inArity; tupleIdx++) {
                int position = (int) Math.floor(idx / (precomputedScale[tupleIdx]) % setCardinality);
                tuple[tupleIdx] = inArray[position];
            }

            skipIndex = pruningCallback.apply(tuple);
            assert (skipIndex <= inArity);
            if (skipIndex == inArity) {
                idx++;
            } else {
                idx += precomputedScale[skipIndex];
            }
        }


        return 1;
    }

    long[] scalesForArity(int inArity, int setCardinality, long resultCardinality) {
        long[] scale = new long[inArity];
        long current = scale[0] = resultCardinality / setCardinality;

        for (int i = 1; i < inArity; i++) {
            current = scale[i] = current / setCardinality;
        }
        return scale;
    }


}
