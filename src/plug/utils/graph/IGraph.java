package plug.utils.graph;

import java.util.Collection;

/**
 * Created by Ciprian TEODOROV on 20/02/17.
 */
public interface IGraph<N, E> {
    Collection<N> getInitialVertices();
    Collection<N> getSources();
    Collection<N> getSinks();
    Collection<N> getVertices();
    Collection<E> getOutgoingEdges(N vertex);
    Collection<E> getIncomingEdges(N vertex);

    Collection<N> fanout(N vertex);
    N getParent(N vertex);

    int getVertexCount();
    int getEdgeCount();
}
