package plug.utils.graph;

import java.util.Collection;

public interface IDirectedHyperEdge<N, D> {
    D getData();
    N getSource();
    Collection<N> getTargets();

    void setData(D data);
    void setSource(N source);
    void setTargets(Collection<N> targets);
}
