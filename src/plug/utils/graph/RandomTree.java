package plug.utils.graph;

import java.util.*;

/**
 * Created by Ciprian TEODOROV on 21/02/17.
 */
public class RandomTree {

    public static Map<Integer, List<Integer>> tree(int v, int maxperlevel, int startID) {
        Random rnd = new Random();
        Map<Integer, List<Integer>> fanout = new HashMap<>();

        fanout.put(startID, new ArrayList<>());

        @SuppressWarnings( "unchecked" )
        List<Integer> nodesAtDepths[] = new List[2];
        nodesAtDepths[0] = new ArrayList<>();
        nodesAtDepths[1] = new ArrayList<>();



        int depth = v;
        int currentIndex = 0;
        int nodeID = startID+1;

        nodesAtDepths[currentIndex].add(startID);

        while (depth > 1) {
            int nodesPerLevel = rnd.nextInt(maxperlevel) + 1;
            for (int i = 0; i<nodesPerLevel; i++) {
                int parent = nodesAtDepths[currentIndex].get(rnd.nextInt(nodesAtDepths[currentIndex].size()));
                int children = nodeID++;

                List<Integer> siblings = fanout.get(parent);
                siblings.add(children);

                nodesAtDepths[1-currentIndex].add(children);
                fanout.put(children, new ArrayList<>());
            }
            nodesAtDepths[currentIndex].clear();
            currentIndex = 1 - currentIndex;

            depth = depth - 1;
        }



        return fanout;
    }
}
