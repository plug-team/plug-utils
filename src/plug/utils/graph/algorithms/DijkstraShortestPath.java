package plug.utils.graph.algorithms;

import plug.utils.graph.IGraph;

import java.util.*;

public class DijkstraShortestPath<N, E> {

    /**
     * <p>Constructs the shortest trace between two nodes, if one exists.
     * @param source source for the trace
     * @param target target for the trace
     * @return a{@link Collection} of {@link N} that start from source and go to target. empty list if such path does not exist
     */
    public List<N> getShortestPath(IGraph<N, E> graph, N source, N target) {
        return getShortestPath(graph, Collections.singletonList(source), target);
    }

    /**
     * <p>Constructs the shortest trace between a node in a source set and a target, if one exists.
     * @param sourceNodes source for the trace
     * @param target target for the trace
     * @return a{@link Collection} of {@link N} that start from source and go to target. empty list if such path does not exist
     */
    public List<N> getShortestPath(IGraph<N, E> graph, Collection<N> sourceNodes, N target) {
        if (target == null) {
            //if there is no target there is no path to it
            return Collections.emptyList();
        }
        if (graph == null || sourceNodes == null || sourceNodes.isEmpty()) {
            //if there is no graph or no source nodes then the only node in the path is the target itself
            return Collections.singletonList(target);
        }
        Map<N, N> predecessors = new HashMap<>();
        Map<N, Integer> distances = new HashMap<>();
        Set<N> visited = new HashSet<>();
        PriorityQueue<NodeCost> candidates = new PriorityQueue<>(10 );
        for (N source : sourceNodes) {
            distances.put(source, 0);
            candidates.add(new NodeCost(source, 0));
        }
        NodeCost current = null;
        while (!candidates.isEmpty()) {
            current = candidates.poll();
            if (current.node == target) break;
            visited.add(current.node);
            for (N next : graph.fanout(current.node)) {
                if (visited.contains(next)) continue;
                int distance = distances.getOrDefault(current.node, Integer.MAX_VALUE);
                distance += 1;
                // if the current distance is infinity or if it is smaller than the new one change it
                if (	!distances.containsKey(next)
                        ||	distances.get(next) > distance) {
                    //change the distance
                    distances.put(next, distance);
                    //record the source of the new distance
                    predecessors.put(next, current.node);
                    candidates.add(new NodeCost(next, distance));
                }
            }
        }
        if (current == null || current.node != target) {
            //if no path is found from the target then the only node in the path is the target itself
            return Collections.singletonList(target);
        }

        //I dont like this path extraction (it takes time)
        LinkedList<N> solution = new LinkedList<>();
        N currentNode = target;
        solution.addFirst(currentNode);
        while (predecessors.containsKey(currentNode)) {
            N parentNode = predecessors.get(currentNode);

            solution.addFirst(parentNode);

            currentNode = parentNode;
        }
        return solution;
    }

    class NodeCost implements Comparable<NodeCost> {
        N node;
        int cost;
        NodeCost(N node, int cost) {
            this.node = node;
            this.cost = cost;
        }

        @Override
        public int compareTo(NodeCost o) {
            if (this.cost < o.cost) return -1;
            if (this.cost == o.cost) return 0;
            return 1;
        }
    }

}
