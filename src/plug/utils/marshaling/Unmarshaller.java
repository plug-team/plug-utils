package plug.utils.marshaling;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;

public class Unmarshaller {

    public static byte[] readData(int size, InputStream stream) throws IOException {
        byte[] data = new byte[size];
        int offset = 0;
        int expectedSize = size;
        do {
            int r = stream.read(data, offset, expectedSize);
            if (r == -1) throw new IOException("End of stream reached " + stream.toString());
            offset += r;
            expectedSize -= r;
        } while (offset < size);
        return data;
    }

    public static int readInt(InputStream stream) throws IOException {
        return ByteBuffer.wrap(readData(4, stream)).order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    public static boolean readBoolean(InputStream stream) throws IOException {
        return ByteBuffer.wrap(readData(1, stream)).order(ByteOrder.LITTLE_ENDIAN).get() > 0;
    }
    public static short readShort(InputStream stream) throws IOException {
        return ByteBuffer.wrap(readData(2, stream)).order(ByteOrder.LITTLE_ENDIAN).getShort();
    }
    public static byte readByte(InputStream stream) throws IOException {
        return ByteBuffer.wrap(readData(1, stream)).order(ByteOrder.LITTLE_ENDIAN).get();
    }
    public static char readChar(InputStream stream) throws IOException {
        return ByteBuffer.wrap(readData(2, stream)).order(ByteOrder.LITTLE_ENDIAN).getChar();
    }
    public static long readLong(InputStream stream) throws IOException {
        return ByteBuffer.wrap(readData(8, stream)).order(ByteOrder.LITTLE_ENDIAN).getLong();
    }
    public static float readFloat(InputStream stream) throws IOException {
        return ByteBuffer.wrap(readData(4, stream)).order(ByteOrder.LITTLE_ENDIAN).getFloat();
    }
    public static double readDouble(InputStream stream) throws IOException {
        return ByteBuffer.wrap(readData(4, stream)).order(ByteOrder.LITTLE_ENDIAN).getDouble();
    }
    public static String readString(InputStream stream) throws IOException {
        int size = readInt(stream);
        return size < 0 ? null : new String(readData(size, stream), StandardCharsets.UTF_8);
    }

    public static String[] readStringArray(InputStream stream) throws IOException {
        int count = Unmarshaller.readInt(stream);
        String[] result = new String[count];

        for (int i = 0; i<count; i++) {
            String atom = Unmarshaller.readString(stream);
            result[i] = atom;
        }
        return result;
    }

    public static byte[] readBuffer(InputStream stream) throws IOException {
        int size = Unmarshaller.readInt(stream);
        return Unmarshaller.readData(size, stream);
    }
}
