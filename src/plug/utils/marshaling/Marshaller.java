package plug.utils.marshaling;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

public class Marshaller {

    public static void write(byte[] array, OutputStream os)  throws IOException {
        os.write(array);
    }

    public static void writeInt(int value, OutputStream os) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putInt(value);
        os.write(buffer.array());
    }

    public static void writeBoolean(boolean value, OutputStream os) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(value? (byte)1 : (byte)0);
        os.write(buffer.array());
    }

    public static void writeShort(short value, OutputStream os) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putShort(value);
        os.write(buffer.array());
    }

    public static void writeByte(byte value, OutputStream os) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(value);
        os.write(buffer.array());
    }

    public static void writeChar(char value, OutputStream os) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putChar(value);
        os.write(buffer.array());
    }

    public static void writeLong(long value, OutputStream os) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putLong(value);
        os.write(buffer.array());
    }

    public static void writeFloat(float value, OutputStream os) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putFloat(value);
        os.write(buffer.array());
    }

    public static void writeDouble(double value, OutputStream os) throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        buffer.putDouble(value);
        os.write(buffer.array());
    }

    public static void writeString(String value, OutputStream os) throws IOException {
        byte[] bytes = value != null ? value.getBytes(StandardCharsets.UTF_8) : null;
        if (bytes != null) {
            writeInt(bytes.length, os);
            write(bytes, os);
        } else {
            writeInt(0, os);
        }
    }

    public static void writeIntArray(int[] values, OutputStream stream) throws IOException {
        Marshaller.writeInt(values.length, stream);
        for (int i : values) {
            Marshaller.writeInt(i, stream);
        }
    }

    public static void writeBooleanArray(boolean[] values, OutputStream stream) throws IOException {
        Marshaller.writeInt(values.length, stream);
        for (boolean i : values) {
            Marshaller.writeBoolean(i, stream);
        }
    }

    public static void writeBuffer(byte[] buffer, OutputStream stream) throws IOException {
        Marshaller.writeInt(buffer.length, stream);
        Marshaller.write(buffer, stream);
    }

    public static void writeBuffers(Collection<byte[]> buffers, OutputStream stream) throws IOException {
        Marshaller.writeInt(buffers.size(), stream);
        for (byte[] buffer : buffers) {
            writeBuffer(buffer, stream);
        }
    }
}
